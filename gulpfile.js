'use strict';

var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify-incremental');
var exposify = require('exposify');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var jshint = require('gulp-jshint');
var ngify = require('ngify');
var browserSync = require('browser-sync');
var shell = require('gulp-shell');

var sources = [
  '*.js', '!force5.js', '!gulpfile.js',
  'core/**/*.js'
];

exposify.config = {
  'angular': 'angular'
};

gulp.task('jshint', [], function () {
  return gulp.src(sources)
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'));
});

var bundler = browserify({
  entries: ['./index.js' ],
  debug: true,
  cache: {},
  packageCache: {},
  fullPaths: true
});
bundler.transform(exposify);
bundler.transform(ngify, {
  moduleName: 'force5'
});

gulp.task('force5.js', ['jshint'], function () {
  return bundler.bundle()
    .on('error', function (err) {
      console.log(err.message);
      this.emit('end');
    })
    .pipe(source('force5.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(uglify({ output: { beautify: true, indent_level: 2 } }))
    .pipe(sourcemaps.write('./', {includeContent: false, sourceRoot: ''}))
    .pipe(gulp.dest('.'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('watch', [], function () {
  gulp.watch(sources.concat(['index.html', 'template/**/*.html']), ['force5.js']);
  return gulp.start('force5.js');
});

gulp.task('run', [], function () {
  browserSync({
    server: { baseDir: './' },
    port: 3000,
    online: false,
    injectChanges: false,
    open: false,
    logPrefix: 'Force5'
  });
  return gulp.start('watch');
});

gulp.task('docs', [], shell.task([
  'node_modules/jsdoc/jsdoc.js ' +
  //'--debug ' +
  //'--explain ' +
  '-c node_modules/angular-jsdoc/conf.json ' +
  '-t node_modules/angular-jsdoc/template ' +
  '-d docs ' +
  '-r core ' +
  'index.js'
]));

gulp.task('doc-watch', ['docs'], function () {
  return gulp.watch(sources, ['docs']);
});

gulp.task('default', ['force5.js']);