'use strict';

var _ = require('lodash');
/*jshint -W079*/ var Promise = require('bluebird');
var Howl = require('howler/src/howler.core.js').Howl;

function Resource () {
  this.image    = {};
  this.sound    = {};
  this.promises = {};
}

Resource.prototype.loadImage = function (name, url) {
  // TODO: Test that src can actually match the url
  if(this.promises['image.' + name] && this.image[name].src === url) {
    return this.promises['image.' + name];
  } else {
    var image = this.image[name] = new Image();
    return (image.promise = this.promises['image.' + name] = new Promise(function (resolve, reject) {
      image.name = name;
      image.onload = function () { resolve(image); };
      image.onerror = reject;
      image.src = url;
    }));
  }
};

Resource.prototype.loadImages = function (images) {
  var self = this;
  return Promise.all(_.map(images, function (url, name) {
    return self.loadImage(name, url);
  }));
};

Resource.prototype.loadSound = function (name, urls, opts) {
  // TODO: Test to make sure that src is accessible
  if(this.promises['sound.' + name] && _.isEqual(this.sound[name].src, urls)) {
    return this.promises['sound.' + name];
  } else {
    if(!opts) opts = {};
    opts = _.merge({ src: urls }, opts);
    delete opts.html5; // FIXME: Requires server support for partial content

    var sound = this.sound[name] = new Howl(opts);
    return (sound.promise = this.promises['sound.' + name] = new Promise(function (resolve, reject) {
      sound.onload      = function () { resolve(sound); };
      sound.onloaderror = reject;
    }));
  }
};

Resource.prototype.loadMusic = function (name, urls, opts) {
  if(!opts) opts = {};
  opts.loop = true;
  return this.loadSound(name, urls, opts);
};

Resource.prototype.resolve = function () {
  return Promise.all(_.values(this.promises));
};

module.exports = Resource;