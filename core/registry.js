'use strict';

var _ = require('lodash');
/*jshint -W079*/ var Promise = require('bluebird');

function Registry ($injector, controller) {
  Object.defineProperties(this, {
    $injector : { value: $injector  },
    controller: { value: controller }
  });
}

Registry.prototype.cleanup = function (root) {
  var self = this;
  return new Promise(function (resolve) {
    if(!root) resolve(root);
    else {
      var obj, result = [], children = [root];
      while (children.length > 0) {
        obj = children.pop();

        // Clean up the object behavior
        if(obj.$behavior) {
          _.forEach(obj.$behavior, /* jshint -W083 */ function (behavior) {
            if(behavior.$ref.destroy) result.push(Promise.method(behavior.$ref.destroy)(obj, self.controller));
          });
        }

        // Cascade into the <typeof object> children
        _.forEach(obj, function (value) {
          if(_.isObject(value)) children.push(value);
        });
      }
      resolve(Promise.all(result));
    }
  });
};

Registry.prototype.applyBehavior = function (name, args, ns, root) {
  args = (args) ? (_.isArray(args) ? args : [args]) : [];
  var self = this;
  var obj, init, func, initDone, updateDone, result = [];
  var children = ((root) ? (_.isArray(root) ? root : [root]) : _.values(self));

  while(children.length > 0) {
    obj = children.pop();

    if(obj.$behavior) {
      // Resolve behaviors
      init = [];
      _.forEach(obj.$behavior, /*jshint -W083*/ function (behavior) {
        if(behavior.service && !behavior.$ref) {
          behavior.$ref = self.$injector.get(behavior.service, 'Registry');
          init.push(behavior);
        }
      });
      (function (init, obj) {
        // Run init as needed in $priority order
        result.push(new Promise(function (resolve) {
          initDone = [];
          if(init.length > 0) {
            init = _.sortBy(init, function (behavior) { return behavior.$ref.$priority; });
            _.forEach(init, function (behavior) {
              initDone.push(Promise.method(behavior.$ref.init)(
                obj, self.controller, behavior.config || behavior.options || {}));
            });
          }
          resolve(Promise.all(initDone));
        }).then(function () {
            // If we ran init, the behaviors need sorting
            if(init.length > 0) obj.$behavior = _.sortBy(obj.$behavior, function (behavior) {
              return behavior.$ref.$priority;
            });

            // Run each update
            updateDone = [];
            _.forEach(obj.$behavior, function (behavior) {
              func = (ns) ? (behavior.$ref[ns] && behavior.$ref[ns][name]) : behavior.$ref[name];
              if(func) {
                updateDone.push(Promise.method(func).apply(behavior.$ref, [obj, self.controller].concat(args)));
              }
            });
            return Promise.all(updateDone);
          }));
      })(init, obj); // Close over these variables to make sure they're correct on each loop
    }

    // Cascade into the <typeof object> children
    _.forEach(obj, /*jshint -W083 */ function (value) {
      if(_.isObject(value)) children.push(value);
    });
  }

  return Promise.all(result);
};

Registry.prototype.applyFunction = function (func, args, root) {
  var self = this;
  var obj, result = [], children = (root && [root]) || _.values(self);
  while(children.length > 0) {
    obj = children.pop();
    result.push(Promise.method(func).apply(func, [obj, self.controller].concat(args)));
    _.forEach(obj, /* jshint -W083 */ function(value) {
      if(_.isObject(value)) children.push(value);
    });
  }
  return Promise.all(result);
};

module.exports = Registry;