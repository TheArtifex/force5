'use strict';
var angular = require('angular');

module.exports = angular.module('force5.core.AutoEngine', [
  require('./engine/index')
])
  .service('force5.core.AutoEngine', AutoEngine)
  .name;

/** @ignore */
AutoEngine.$inject = ['force5.core.engine.CanvasEngine'];

/**
 * @summary Delegating engine service.
 * @description
 * Defines an angular service that selects between the various engine implementations by querying the capabilities
 * of the browser and selecting the most capable engine. This will eventually select engines in the following order:
 * webgl -> canvas -> dom. Currently, only the canvas renderer is implemented, so it is always selected.
 *
 * This class functions as a psuedo-proxy by returning an alternate object from its constructor. The resulting object
 * passed around by angular will be whatever this return value is, allowing the service to "proxy" another by
 * returning it. In the future (es6), this should be converted to a proper [Proxy]{@link external:Proxy}
 * when its available.
 *
 * @ngdoc service
 * @name force5.core#AutoEngine
 * @requires CanvasEngine
 * @param {CanvasEngine} CanvasEngine The CanvasEngine service.
 * @returns {Engine} The engine proxy instance.
 * @constructor
 */
function AutoEngine (CanvasEngine) {
  // TODO: Implement the WebGL and DOM engines
  // TODO: Select the proxy based on detection (modernizr?)
  var proxy = CanvasEngine;
  return proxy;
}