/** @module core/stage */
'use strict';

/**
 * @summary A game stage.
 * @description
 * A single stage within the force5 framework. Serves as a base class for stage services to provide default
 * implementations of the required functions and properties.
 *
 * Stages function as singletons. A system context is passed into every function; context specific (local) variables
 * should be stored on the context, as stage variables are shared across all contexts utilizing the same stage.
 *
 * Stages have a distinct lifecycle within the game framework. All stages are
 * [initalized]{@link module:core/stage~Stage#init} once for each system context when they are first used
 * (as triggered by an [changeStage]{@link Force5Controller#changeStage} call). If an existing stage is in use,
 * it is [left]{@link module:core/stage~Stage#leave} first, and then the new stage is
 * [entered]{@link module:core/stage~Stage#enter}.
 *
 * During normal execution, the active stage is given a chance to execute during the update and render steps of the
 * game loop. These methods are provided for stage specific code execution as needed. However, if the code could
 * possibly be reused, or is non specific to the stage, consider implementing it as a
 * [Behavior]{@link module:core/behavior~Behavior}, and implementing methods in the [system]{@link Behavior.system}
 * namespace.
 *
 * @constructor
 */
function Stage () {
  /**
   * The url of the heads-up display partial that should be rendered for this stage. The system ensures that
   * this template is loaded when the stage is selected. This property must be specified for each service. While
   * verbose, this makes no assumptions about hud interitance between stages; if that is desired, it must be
   * defined that way. The system context does special management of this property to ensure that the hud does not
   * reload between every stage; only as needed when the new stage uses a new hud definition.
   *
   * The angular $templateCache can be used to pre-populate the system with hud definitions for this purpose. If a
   * hud is unneeded, the template 'force5-blank.html' is pre-populated for this purpose.
   *
   * @default
   * @type {string}
   */
  this.templateUrl = 'force5-blank.html';
}

/**
 * Initialize the stage in the given system context.
 *
 * @param {Force5Controller} f5 The system context.
 */
Stage.prototype.init = function () {};
Stage.prototype.enter = function () {};
Stage.prototype.leave = function () {};
Stage.prototype.update = function () {};
Stage.prototype.render = function () {};

module.exports = Stage;