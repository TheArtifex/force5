/**
 * @summary Force5 core components.
 * @description
 * Defines the core services and directives utilized by the framework.
 * @name force5.core
 * @module force5.core
 * @ngdoc module
 */
'use strict';
var angular = require('angular');
module.exports = angular.module('force5.core', [
  require('./force5'),
  require('./style'),
  require('./engine/index'),
  require('./behavior/index'),
  require('./default-stage'),
  require('./auto-engine'),
  require('./system'),
  require('./music')
]).name;