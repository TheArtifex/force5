'use strict';
var angular = require('angular');
/*jshint -W079*/ var Promise = require('bluebird');
var Howler  = require('howler/src/howler.core.js').Howler;

module.exports = angular.module('force5.core.Music', [])
  .service('force5.core.Music', Music)
  .name;

function Music () {
  this.duration = 1.0;
  this.current  = Promise.resolve();
  this.sprite   = undefined;
}

Music.prototype.play = function (newSound, opts) {
  var self = this;
  if(!opts) opts = {};
  return (this.current = this.current.then(function (oldSound) {
    return new Promise(function (resolve) {
      function playSound (sound) {
        if(sound) {
          if(opts.immediate) {
            sound.once('play', function () {
              resolve(sound);
            });
            self.sprite = sound.play(opts.sprite);
          } else {
            sound.volume(0.0);
            self.sprite = sound.play(opts.sprite);
            sound.pause(self.sprite);
            sound.once('faded', function () {
              resolve(sound);
            });
            sound.seek(0, self.sprite)
              .fade(0.0, (oldSound && oldSound.volume()) || opts.volume || 1.0, opts.duration || self.duration, self.sprite);
          }
        } else resolve();
      }
      function playNewSound () { playSound(newSound); }
      function playOldSound () { playSound(oldSound); }

      if(newSound !== oldSound) {
        if(oldSound && oldSound.playing()) {
          if(opts.order ) oldSound.once('faded', playNewSound);
          oldSound.fade(oldSound.volume(), 0.0, opts.duration || self.duration);
          if(!opts.order) playNewSound();
        }
        else playNewSound();
      }
      else if(oldSound && !oldSound.playing()) playOldSound();
      else return oldSound;
    });
  }));
};

Music.prototype.pause = function(opts) {
  var self = this;
  if(!opts) opts = {};
  return this.current.then(function (sound) {
    return new Promise(function (resolve) {
      if(sound) {
        if(opts.immediate) {
          sound.once('pause', function () {
            resolve(sound);
          });
          sound.pause(self.sprite);
        }
        else {
          sound.once('faded', function () {
            resolve(sound);
          });
          sound.fade(sound.volume(), 0.0, opts.duration || self.duration, self.sprite);
        }
      } else resolve();
    });
  });
};

Music.prototype.stop = function (opts) {
  var self = this;
  if(!opts) opts = {};
  return this.current.then(function (sound) {
    return new Promise(function (resolve) {
      function stopSound() {
        sound.once('end', function () {
          resolve(sound);
        });
        sound.stop(self.sprite);
      }

      if(sound) {
        if(opts.immediate) {
          stopSound();
        } else {
          sound.once('faded', function () {
            stopSound();
          });
          sound.fade(sound.volume(), 0.0, opts.duration || self.duration, self.sprite);
        }
      } else resolve();
    });
  });
};

Music.prototype.seek = function (seek) {
  var self = this;
  return this.current.then(function (sound) {
    return (sound) ? sound.seek(seek, self.sprite) : -1;
  });
};

Music.prototype.loop = function (loop) {
  var self = this;
  return this.current.then(function (sound) {
    return (sound) ? sound.loop(loop, self.sprite) : false;
  });
};

Music.prototype.volume = function (volume) {
  var self = this;
  return this.current.then(function (sound) {
    return (sound) ? sound.volume(volume, self.sprite) : 0;
  });
};

Music.prototype.mute = function (mute) {
  var self = this;
  return this.current.then(function (sound) {
    return (sound) ? sound.mute(mute, self.sprite) : false;
  });
};

Music.prototype.duration = function () {
  return this.current.then(function (sound) {
    return new Promise(function (resolve) {
      if(sound) {
        if(!sound._loaded) {
          sound.once('load', function () {
            resolve();
          });
        } else resolve();
      }
    }).then(function () {
      return sound.duration();
    });
  });
};

Music.prototype.on = function (event, fn) {
  var self = this;
  return this.current.then(function (sound) {
    if(sound) {
      sound.on(event, fn, self.sprite);
      return sound;
    }
  });
};

Music.prototype.off = function (event, fn) {
  var self = this;
  return this.current.then(function (sound) {
    if(sound) {
      sound.off(event, fn, self.sprite);
      return sound;
    }
  });
};

Music.prototype.once = function (event, fn) {
  var self = this;
  return this.current.then(function (sound) {
    sound.once(event, fn, self.sprite);
    return sound;
  });
};

Music.prototype.system = {};
Music.prototype.system.mute = function(mute) {
  return Howler.mute(mute);
};
Music.prototype.system.volume = function(volume) {
  return Howler.volume(volume);
};
