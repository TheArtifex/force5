'use strict';
var util = require('util');
var angular = require('angular');
var Stage = require('./stage');

module.exports = angular.module('force5.core.DefaultStage', [])
  .service('force5.core.DefaultStage', DefaultStage)
  .name;

/**
 * @summary A default stage implementation.
 * @description
 * Exposes the base [Stage]{@link module:core/stage~Stage} class as a default implementation. Used by the
 * [Force5Controller]{@link Force5Controller} when an initial stage is not defined for the context.
 *
 * @constructor
 * @name force5.core#DefaultStage
 * @extends module:core/stage~Stage
 * @ngdoc service
 */
function DefaultStage () {
  Stage.call(this);
}

util.inherits(DefaultStage, Stage);