'use strict';
var util    = require('util');
var angular = require('angular');
var Engine  = require('../engine');

module.exports = angular.module('force5.core.engine.CanvasEngine', [])
  .service('force5.core.engine.CanvasEngine', CanvasEngine)
  .name;

function CanvasEngine () {
  Engine.call(this);
  this.templateUrl = 'force5-engine-canvas.html';
}

util.inherits(CanvasEngine, Engine);

CanvasEngine.prototype.context = function (f5, element) {
  var canvas, ctx;

  // Find the local canvas element
  canvas = element.find('canvas');
  if(canvas.length < 1) throw new Error('Could not resolve canvas element');

  // Get the 2d context (possibly throws errors)
  ctx = canvas[0].getContext('2d');

  // Return the arguments to pass to start and render
  return [ctx, canvas];
};

CanvasEngine.prototype.start = function(f5, ctx) {
  ctx.clearRect(0, 0, f5.width, f5.height);
};

CanvasEngine.prototype.render = function (object, f5, ctx) {
  if(!f5.resource.image[object.image]) return; // TODO this is a bug
  ctx.save();
  ctx.translate(object.x, object.y);
  ctx.rotate(object.r);
  ctx.drawImage(f5.resource.image[object.image], -object.radius, -object.radius, 2*object.radius, 2*object.radius);
  ctx.restore();
};