'use strict';

var Mouse    = require('./input/mouse');
var Keyboard = require('./input/keyboard');

function Input () {
  this.mouse = new Mouse();
  this.keys  = new Keyboard();
}

module.exports = Input;