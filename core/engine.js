'use strict';

function Engine () {
  this.templateUrl = 'force5-blank.html';
}

Engine.prototype.init  = function () {};
Engine.prototype.start = function () {};

Engine.prototype.context = function () {
  throw new Error('Engine context not defined');
};

Engine.prototype.render = function () {
  throw new Error('Engine render not defined');
};

module.exports = Engine;