'use strict';
var _ = require('lodash');
var angular = require('angular');
/*jshint -W079*/ var Promise = require('bluebird');

var Registry = require('./registry');
var Resource = require('./resource');
var Input    = require('./input');

module.exports = angular.module('force5.core.Force5', [])
  .directive('force5', Force5)
  .name;

Force5.$inject = ['$document'];

var directiveCount = 0;

function Force5 ($document) {
  var directive = {
    restrict: 'EA',
    scope: {
      width:  '@',
      height: '@',
      stage:  '@',
      engine: '@',
      title:  '@'
    },
    controller: Force5Controller,
    controllerAs: 'f5',
    templateUrl: 'force5.html',
    link: link
  };

  function link (scope, element) {
    directiveCount++;

    // Focus on the current element if a force5 instance isn't already
    var div;
    if(!angular.element($document[0].activeElement).hasClass('force5')) {
      div = element.find('div');
      if(div.length > 0) div[0].focus();
    }
  }

  return directive;
}

Force5Controller.$inject = [
  '$scope', '$element', '$injector', '$document',
  'force5.core.DefaultStage', 'force5.core.AutoEngine',
  'force5.core.System', 'force5.core.Music'
];

function Force5Controller (
  $scope, $element, $injector, $document,
  defaultStage, defaultEngine,
  System, Music
) {
  var f5 = this;

  Object.defineProperty(f5, 'multiple', { get: function () { return directiveCount > 1; } });

  $scope.$watch('width' , function (newValue, oldValue) {
    if(!f5.width || newValue !== oldValue) {
      try {
        f5.width = (newValue) ? parseInt(newValue) : 1920;
      } catch (e) { f5.width = 1920; }
    }
  });
  $scope.$watch('height', function (newValue, oldValue) {
    if(!f5.height || newValue !== oldValue) {
      try {
        f5.height = (newValue) ? parseInt(newValue) : 1080;
      } catch (e) { f5.height = 1080; }
    }
  });
  $scope.$watch('stage' , function (newValue, oldValue) {
    if(!f5.stage || newValue !== oldValue) {
      changeStage(newValue);
    }
  });
  $scope.$watch('engine', function (newValue, oldValue) {
    if(!f5.engine || newValue !== oldValue) {
      setEngine(newValue);
    }
  });
  $scope.$watch('title',  function (newValue, oldValue) {
    if(!f5.title || newValue !== oldValue) {
      setTitle(newValue);
    }
  });

  f5.registry = new Registry($injector, f5);
  f5.resource = new Resource();
  f5.input    = new Input();

  f5.music = {};
  _.forEach(Music.constructor.prototype, function (value, prop) {
    if(_.isFunction(value)) f5.music[prop] = _.bind(value, Music);
    else f5.music[prop] = value;
  });
  f5.music.play = function (id, opts) {
    return Promise.resolve().then(function () {
      var sound = f5.resource.sound[id];
      if(sound) {
        return sound.promise.then(function (sound) {
          return Music.play(sound, opts);
        });
      } else throw new Error('Sound id \'' + id + '\' does not exist');
    });
  };

  f5.setTitle    = setTitle;
  f5.setSize     = setSize;
  f5.changeStage = changeStage;
  f5.setEngine   = setEngine;

  var prev, context, stageInit = {};
  var stagePromise  = Promise.resolve();
  var enginePromise = Promise.resolve();

  nextLoop();

  function setSize (width, height) {
    if(!f5.width  || (width  && f5.width  !== width )) f5.width  = width;
    if(!f5.height || (height && f5.height !== height)) f5.height = height;
  }

  function setTitle (name) {
    if(!f5.title || (name && f5.title !== name)) {
      var title = angular.element($document).find('title');
      f5.title = name || (title.length > 0 && title.text()) || 'Force5';
      if(!f5.multiple) {
        if(title.length < 1) {
          var head = angular.element($document).find('head');
          title = angular.element('<title></title>');
          head.append(title);
        }
        title.text(f5.title);
      } else throw new Error('setTitle is disabled with multiple components');
    }
  }

  function changeStage (stage) {
    if(!f5.stage || (stage && f5.stage !== stage)) {
      stagePromise = new Promise(function (resolve) {
        if(f5.stage) {
          resolve(
            System.apply('leave', [f5])
              .then(function () {
                return Promise.method(f5.stage.leave)(f5);
              })
          );
        }
        else resolve();
      })
        .then(function () {
          // Connect to the new stage (triggers view changes)
          f5.stage = (stage)
            ? $injector.get(stage, 'force5.core.Force5')
            : defaultStage;
        })
        .then(function () {
          if(!stage) stage = 'default';
          return Promise.resolve().then(function () {
            if(!stageInit[stage]) {
              stageInit[stage] = true;
              return System.apply('init', [f5])
                .then(function () {
                  return Promise.method(f5.stage.init)(f5);
                });
            }
          });
        })
        .then(function () {
          return System.apply('enter', [f5])
            .then(function () {
              return Promise.method(f5.stage.enter)(f5);
            });
        });
    }
    return stagePromise;
  }

  function setEngine (engine) {
    if(!f5.engine || (engine && f5.engine !== engine)) {
      enginePromise = Promise.resolve()
        .then(function () {
          f5.engine = (engine)
            ? $injector.get(engine, 'force5.core.Force5')
            : defaultEngine;
        })
        .then(function () {
          return new Promise(function (resolve) {
            $scope.$on('$includeContentLoaded', function () {
              resolve($element);
            });
          });
        })
        .then(function (element) {
          return Promise.method(f5.engine.context)(f5, element);
        })
        .then(function (args) {
          context = args;
          return Promise.method(f5.engine.init).apply(f5.engine, [f5].concat(args));
        });
    }
    return enginePromise;
  }

  function update(dt) {
    return System.apply('update', [f5, dt])
      .then(function () {
        return new Promise(function(resolve, reject) {
          try {
            return resolve(f5.stage.update(f5, dt));
          } catch(e) {
            reject(e);
          }
        });
      })
      .then(function () {
        return f5.registry.applyBehavior('update', [dt]);
      });
  }

  function render () {
    return f5.resource.resolve()
      .then(function () {
        return Promise.method(f5.engine.start).apply(f5.engine, [f5].concat(context));
      })
      .then(function () {
        return System.apply('render', [f5]);
      })
      .then(function () {
        return Promise.method(f5.stage.render)(f5);
      })
      .then(function () {
        return f5.registry.applyFunction(f5.engine.render, context);
      });
  }

  function nextLoop() {
    enginePromise
      .then(function () {
        return stagePromise;
      })
      .then(function () {
        requestAnimationFrame(gameLoop);
      });
  }

  function gameLoop (curr) {
    if(prev) {
      update(curr - prev)
        .then(render)
        .then(nextLoop);
    }
    else nextLoop();
    prev = curr;
  }
}