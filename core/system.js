'use strict';
var _ = require('lodash');
var angular = require('angular');
/*jshint -W079*/ var Promise = require('bluebird');

module.exports = angular.module('force5.core.System', [])
  .service('force5.core.System', System)
  .name;

function System () {
  this.services = [];
}

function systemPriority(svc) {
  return svc.system.$priority;
}

System.prototype.register = function (service) {
  // Register the service by its priority
  this.services.splice(_.sortedIndex(this.services, service, systemPriority), 0, service);
};

System.prototype.apply = function (name, args) {
  // Call a method on every registered service
  var result = [];
  _.forEach(this.services, function (service) {
    if(service.system && service.system[name]) {
      result.push(Promise.method(service.system[name]).apply(service, args));
    }
  });
  return Promise.all(result);
};