'use strict';
var _ = require('lodash');
var angular = require('angular');

module.exports = angular.module('force5.core.Style', [])
  .directive('f5Style', F5Style)
  .name;

function F5Style() {
  var directive = {
    restrict: 'A',
    link: link
  };

  function link (scope, element) {
    element.remove();

    var found = _.some(angular.element(document).find('style'), function (style) {
      return angular.element(style).attr('f5-style') !== undefined;
    });

    if(!found) {
      angular.element(document).find('head').prepend(element);
    }
  }

  return directive;
}