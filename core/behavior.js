/** @module core/behavior */
'use strict';

/**
 * @summary An object behavior.
 * @description
 * Defines behavior for an object managed by the force5 framework. Serves as the base class for behavior services to
 * provide default implementations of the required functions.
 *
 * Behaviors function as singletons. A game object and/or system context is passed into every function;
 * context specific (local) variables should be stored in one of these two locations, as behavior variables
 * are shared across all objects utilizing the same behavior.
 *
 * Behaviors have two method namespaces: standard and system:
 *
 * System methods occupy the [system]{@link Behavior.system} namespace; they
 * provide the hook for execution at their named stages prior to execution of the standard namespace. This is primarily
 * to allow execution of system level services as part of the main game loop. For example, if you have a special
 * controller service that provides new input capabilities, you can execute its behavior with these functions instead
 * of the "potted-plant solution" (adding an (invisible) object to the scene graph, and attaching
 * the behavior there).
 *
 * Standard methods are executed on a per-scene graph object basis strictly after system methods execute. Generally,
 * these methods are required to implement your service, but can be omitted as needed to fall back to the defined
 * no-ops below.
 *
 * Execution order of behaviors attached to objects is managed by the
 * [$priority]{@link module:core/behavior~Behavior#$priority} and
 * [system.$priority]{@link Behavior.system.Behavior#system.$priority} values. Set these values to ensure ordered
 * execution of this behavior.
 *
 * Phase execution can be disabled for the behavior by setting keys in
 * [$phase]{@link module:core/behavior~Behavior#$phase}.
 *
 * @constructor
 * @see Behavior.system
 */
function Behavior () {
  /**
   * The phases that this behavior will operate in. If a phase is omitted (undefined), it evaluates to true. To
   * disable a phase, specify the dot-delimited function to omit with a value of false.
   *
   * @type {object}
   */
  this.$phase = {};

  /**
   * The priority of this behavior. Determines when the behavior will run relative to other behaviors; behaviors
   * are run in ascending priority order.
   *
   * @type {number}
   * @default
   */
  this.$priority = 100;

  /**
   * The system priority of this behavior. Determines when this behavior will run relative to other behaviors in the
   * same namespace. Behaviors are run in ascending priority order.
   *
   * @memberof Behavior.system
   * @alias Behavior#system.$priority
   * @type {number}
   * @default
   */
  this.system.$priority = 100;
}

/**
 * Initialize this behavior on the provided object.
 *
 * @param {object} obj The game object.
 * @param {Force5Controller} f5 The system context.
 */
Behavior.prototype.init = function () {};

/**
 * Clean up the behavior on the provided object.
 *
 * @param {object} obj The game object.
 * @param {Force5Controller} f5 The system context.
 */
Behavior.prototype.destroy = function () {};

/**
 * Update the object with this behavior.
 *
 * @param {object} obj The game object.
 * @param {Force5Controller} f5 The system context.
 */
Behavior.prototype.update = function () {};

/**
 * Behavior system namespace
 *
 * @memberof Behavior
 * @namespace system
 */
Behavior.prototype.system = {};

/**
 * Initialize this behavior at the system level. Executed once per stage, before
 * [stage.init]{@link stage.init}.
 *
 * @memberof Behavior.system
 * @param {Force5Controller} f5 The system context.
 */
Behavior.prototype.system.init   = function () {};

/**
 * Execute the behavior when a stage is entered. Executed immediately following a stage change; precedes
 * [stage.enter]{@link stage.enter}
 *
 * @memberof Behavior.system
 * @param {Force5Controller} f5 The system context.
 */
Behavior.prototype.system.enter  = function () {};

/**
 * Execute the behavior when leaving a stage. Executed immediately before a stage change; precedes
 * [stage.leave]{@link stage.leave}
 *
 * @memberof Behavior.system
 * @param {Force5Controller} f5 The system context.
 */
Behavior.prototype.system.leave  = function () {};

/**
 * Execute the behavior during the update step. Precedes
 * [stage.update]{@link stage.update} and the [Registry]{@link Registry} update.
 *
 * @memberof Behavior.system
 * @param {Force5Controller} f5 The system context.
 * @param {number} dt The time delta since the last update step, in milliseconds.
 */
Behavior.prototype.system.update = function () {};

/**
 * Execute the behavior during the render step. Precedes
 * [stage.render]{@link stage.render} and the [Engine]{@link Engine} render.
 *
 * @memberof Behavior.system
 * @param {Force5Controller} f5 The system context.
 */
Behavior.prototype.system.render = function () {};

module.exports = Behavior;