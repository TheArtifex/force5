'use strict';

var _ = require('lodash');
var keycode = require('keycode');

// XXX: The symbols are real cute, and really idiotic
// TODO: Submit a pull request, and shame the developer
keycode.names[16] = 'shift';
keycode.names[17] = 'ctrl';
keycode.names[18] = 'alt';
keycode.names[19] = 'pause';
keycode.names[20] = 'caps';
keycode.names[33] = 'pgup';
keycode.names[34] = 'pgdn';
keycode.names[91] = 'meta';

function Keyboard () {
  this.enabled = true;
  this.alias = {
    ctrl:  ['ctl', 'control'],
    meta:  ['windows', 'command', 'cmd'],
    alt:   ['option'],
    pause: ['break'],
    caps:  ['capslock'],
    esc:   ['escape']
  };
}

Keyboard.prototype.$down = function InputKeyDown (event) {
  if(this.enabled) {
    var key = keycode(event);
    this[key] = true;
    if(this.alias[key]) {
      _.forEach(this.alias[key], function (alias) {
        this[alias] = true;
      }, this);
    }
  }
};

Keyboard.prototype.$up = function InputKeyUp (event) {
  if(this.enabled) {
    var key = keycode(event);
    this[key] = false;
    if(this.alias[key]) {
      _.forEach(this.alias[key], function (alias) {
        this[alias] = false;
      }, this);
    }
  }
};

module.exports = Keyboard;