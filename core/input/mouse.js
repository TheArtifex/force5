'use strict';

var _ = require('lodash');

function Mouse () {
  this.click = { enabled: true  };
  this.move  = { enabled: false };

  this.x = 0;
  this.y = 0;
  this.buttons = {};

  this.alias = { 0: ['left'], 2: ['right'] };
}

Mouse.prototype.$down =
Mouse.prototype.$up   =
Mouse.prototype.$move = function MouseHandler (event) {
  var condition, state, buttons;
  var type = ('' + event.type).toLowerCase();

  switch(type) {
    case 'mousedown':
    case 'mouseup':
      state = (type === 'mousedown');
      buttons = true;
      condition = this.click.enabled; break;
    case 'mousemove':
      buttons = false;
      condition = this.move.enabled; break;
  }

  if(condition) {
    var element = event.target;
    var docE, offset = {}, box = { top: 0, left: 0}, doc = element && element.ownerDocument;
    docE = doc.documentElement;
    if(typeof element.getBoundingClientRect !== 'undefined') {
      box = element.getBoundingClientRect();
    }
    offset.top  = box.top  + (window.pageYOffset || docE.scrollTop ) - (docE.clientTop  || 0);
    offset.left = box.left + (window.pageXOffset || docE.scrollLeft) - (docE.clientLeft || 0);

    this.x = event.clientX - offset.left;
    this.y = event.clientY - offset.top;

    if(buttons) {
      this.buttons[event.button] = state;
      _.forEach(this.alias[event.button], function (name) {
        this.buttons[name] = state;
      }, this);
    }
  }
};

module.exports = Mouse;