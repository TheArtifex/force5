/**
 * The built in Proxy object (es6).
 * @external Proxy
 * @see {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy|Proxy}
 */

/**
 * Force5 game framework
 * @name force5
 * @module force5
 * @ngdoc module
 */
'use strict';
var angular = require('angular');
var Promise = require('bluebird');

Promise.onPossiblyUnhandledRejection(function(e) {
  throw e;
});

module.exports = angular.module('force5', [
  require('./core')
]).name;

require('./template/force5.html');
require('./template/force5-blank.html');
require('./template/force5-engine-canvas.html');